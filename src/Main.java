
public class Main {

	public static void main(String[] args) {
		PiecePart aprimeraParte, asegundaParte, aterceraParte, acuartaParte, aquintaParte, asextaParte;
		PiecePart bprimeraParte, bsegundaParte, bterceraParte, bcuartaParte, bquintaParte, bsextaParte;
		aprimeraParte = new PiecePart(5, 0);
		asegundaParte = new PiecePart(5, 1);
		aterceraParte = new PiecePart(5, 2);
		acuartaParte = new PiecePart(5, 3);
		aquintaParte = new PiecePart(5, 4);
		asextaParte = new PiecePart(6, 2);
		
		bprimeraParte = new PiecePart(8, 0);
		bsegundaParte = new PiecePart(8, 1);
		bterceraParte = new PiecePart(8, 2);
		bcuartaParte = new PiecePart(8, 3);
		bquintaParte = new PiecePart(8, 4);
		bsextaParte = new PiecePart(7, 2);
		
		Piece a = new Piece(asextaParte, aprimeraParte, asegundaParte, aterceraParte, acuartaParte, aquintaParte, asextaParte);
		Piece b = new Piece(bsextaParte, bprimeraParte, bsegundaParte, bterceraParte, bcuartaParte, bquintaParte, bsextaParte);
		
		for(PiecePart pp:a.getPieceParts()) {
			pp.setWidth(100);
			pp.setHeight(100);
		}
		for(PiecePart pp:b.getPieceParts()) {
			pp.setWidth(100);
			pp.setHeight(100);
		}

		
		Main.graphicTestRotation(a, true, 4, 1000);
		Main.graphicTestRotation(a, false, 4, 1000);

	}
	
	public static void testRotation(Piece piece, boolean direction) {
		System.out.print("Rotando pieza hacia la");
		if(direction) System.out.println(" izquierda"); else System.out.println(" derecha");
		System.out.println(piece);
		System.out.println(90);
		if(direction) piece.rotateLeft(); else piece.rotateRight();
		System.out.println(piece);
		System.out.println("180");
		if(direction) piece.rotateLeft(); else piece.rotateRight();
		System.out.println(piece);
		System.out.println(270);
		if(direction) piece.rotateLeft(); else piece.rotateRight();
		System.out.println(piece);
		System.out.println(360);
		if(direction) piece.rotateLeft(); else piece.rotateRight();
		System.out.println(piece);
	}
	
	public static void graphicTestRotation(Piece p, boolean direction, int times, long pauseTime) {
		TetrisGrid grid = new TetrisGrid(10, 10);
		grid.addPiece(p);
		TetrisRender ren = new TetrisRender(grid);
		try {
			Thread.sleep(pauseTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < times; i++) {
			ren.paintAll();
			try {
				Thread.sleep(pauseTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(direction) p.rotateLeft(); else p.rotateRight();
		}
		ren.closeAllWindows();
	}
	
	public static <T> void printBdmArr(T[][] arr) {
		for(T[] i:arr) {
			for(T j:i) {
				System.out.print(j + " ");
			}
			System.out.println("");
		}
	}
	
}
