
public enum PartTypes {

	NORMAL_BLOCK(200);
	
	private int code;
	
	PartTypes(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
}
