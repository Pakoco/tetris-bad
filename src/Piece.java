import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class Piece {

	private ArrayList<PiecePart> parts = new ArrayList<PiecePart>();
	private PiecePart center;
	
	private PieceTypes type;
	
	public static Piece fuse(Piece a, Piece b, PiecePart newCenter) {
		return new Piece(newCenter, Stream.concat(Arrays.stream(a.getPieceParts()), Arrays.stream(b.getPieceParts()))
        .toArray(PiecePart[]::new));
	}
	
	public static Piece getInstance(PieceTypes typeToInstance, int x, int y) {
		Piece piece;
		PiecePart center;
		switch(typeToInstance) {
			case CUSTOM:
				piece = new Piece(null);
				break;
			case NORMAL_BLOCK:
				piece = new Piece(null,
						new PiecePart(x, y),
						new PiecePart(x - 1, y),
						new PiecePart(x, y - 1),
						new PiecePart(x - 1, y - 1));
				break;
			case NORMAL_LINE:
				center = new PiecePart(x, y);
				piece = new Piece(center, center,
						new PiecePart(x, y + 1),
						new PiecePart(x, y - 1),
						new PiecePart(x, y - 2));
				break;
			default:
				piece = null;
				break;
		}
		return piece;
	}
	
	public Piece(PiecePart center, PiecePart ... parts) {
		this.center = center;
		for(PiecePart p : parts) {
			this.parts.add(p);
		}
		type = PieceTypes.CUSTOM;
	}
	
	public PiecePart[] getPieceParts() {
		return parts.toArray(new PiecePart[parts.size()]);
	}
	
	public PieceTypes getType() {
		return type;
	}
	
	public void move(int x, int y) {
		for(PiecePart p:parts) {
			p.setX(p.getX() + x);
			p.setY(p.getY() + y);
		}
	}
	
	/**
	 * Rotates the piece to the right.
	 * */
	public void rotateRight() {
		if(center != null) {
			int narrow, x;
			for(PiecePart p:parts) {
				if(p != center) {
					narrow = center.getY() - p.getY();
					x = p.getX() - center.getX();
					p.setX(center.getX() + narrow);
					p.setY(x + center.getY());
				}
			}
		}
	}
	
	/**
	 * Rotates the piece to the left.
	 * */
	public void rotateLeft() {
		if(center != null) {
			int narrow, y;
			for(PiecePart p:parts) {
				if(p != center) {
					narrow = center.getX() - p.getX();
					y = p.getY() - center.getY();
					p.setY(center.getY() + narrow);
					p.setX(y + center.getX());
				}
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(PiecePart p:parts) {
			sb.append(p.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
	
}
