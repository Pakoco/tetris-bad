
public class PiecePart {

	private int x, y;
	private int width, height;
	
	private PartTypes type;
	
	public PiecePart(int x, int y) {
		this(x, y, 0, 0);
	}
	
	public PiecePart(int x, int y, int width, int height) {
		this(x, y, width, height, PartTypes.NORMAL_BLOCK);
	}
	
	public PiecePart(int x, int y, int width, int height, PartTypes type) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.type = type;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public PartTypes getBlockType() {
		return type;
	}
	
	public void setBlockType(PartTypes type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "[x = " + x + ", y = " + y + "]";
	}
	
}
