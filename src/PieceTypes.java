
public enum PieceTypes {

	CUSTOM(0),
	NORMAL_LINE(1),
	NORMAL_BLOCK(2);
	
	private int code;
	
	PieceTypes(int code) {
		this.code = code;
	}
	
}
