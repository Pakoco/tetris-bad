import java.util.ArrayList;

public class TetrisGrid {
	
	public final int columns, rows;
	
	Piece[][] grid;
	
	private ArrayList<Piece> pieces = new ArrayList<Piece>();
	PiecePart[][] orderedParts; // Las partes de las piezas denrto del grid
	
	public TetrisGrid(int columns, int rows) {
		this.columns = columns;
		this.rows = rows;
		grid = new Piece[columns][rows];
		orderedParts = new PiecePart[columns][rows];
	}
	
	public Piece[][] getPiecesGrid() {
		return grid;
	}
	
	public PiecePart[][] getPiecePartGrid() {
		return orderedParts;
	}
	
	public Piece[] getPieces() {
		return pieces.toArray(new Piece[pieces.size()]);
	}
	
	public void fuse(Piece pieceA, Piece pieceB, PiecePart center) {
		if(pieces.contains(pieceA)) pieces.remove(pieceA);
		if(pieces.contains(pieceB)) pieces.remove(pieceB);
		Piece union = Piece.fuse(pieceA, pieceB, center);
		addPiece(union);
	}
	
	public void fuseAll() {
		Piece union = new Piece(null);
		for(Piece p:pieces) {
			if(pieces.contains(p)) pieces.remove(p);
			union = Piece.fuse(union, p, null);
		}
		addPiece(union);
	}
	
	public void addPiece(Piece piece) {
		PiecePart[] parts = piece.getPieceParts();
		pieces.add(piece);
		for(PiecePart p:parts) {
			grid[p.getX()][p.getY()] = piece;
			orderedParts[p.getX()][p.getY()] = p;
		}
	}
	
	public void update(Piece[] updatedPieces) {
		clear();
		for(Piece p:updatedPieces) {
			addPiece(p);
		}
	}
	
	public void clear() {
		pieces = new ArrayList<Piece>();
		grid = new Piece[columns][rows];
		orderedParts = new PiecePart[columns][rows];
	}
	
}
