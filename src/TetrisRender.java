import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

public class TetrisRender {

	private TetrisGrid grid;
	
	private Map<PiecePart, JFrame> graphicPieces;
	
	private Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	private int margin;
	
	private boolean autoPieceSize = true;
	
	public TetrisRender(TetrisGrid grid) {
		this(grid, 0);
		margin = (int)(screenSize.getWidth() * 100 / 55);
	}
	
	public TetrisRender(TetrisGrid grid, int margin) {
		this.grid = grid;
		graphicPieces = new HashMap<PiecePart, JFrame>();
		this.margin = margin;
	}
	
	public void paintAll() {
		JFrame frame;
		for(Piece p:grid.getPieces()) {
			for(PiecePart pp:p.getPieceParts()) {
				if(!graphicPieces.containsKey(pp)) {
					graphicPieces.put(pp, createFrame(pp));
				} else {
					frame = graphicPieces.get(pp);
					updateFrame(frame, pp);
				}
			}
		}
	}
	
	public void closeAllWindows() {
		for(JFrame f:graphicPieces.values()) {
			f.dispose();
		}
	}
	
	public int getMargin() {
		return margin;
	}
	
	public void setAutoPieceSize(boolean autoPieceSize) {
		this.autoPieceSize = autoPieceSize;
	}
	
	JFrame createFrame(PiecePart pp) {
		JFrame frame = new JFrame();
		if(autoPieceSize) {
			
		} else {
			frame.setSize(margin + pp.getWidth(), margin + pp.getHeight());
			frame.setLocation(margin + pp.getX() * pp.getWidth(), margin + pp.getY() * pp.getHeight());
		}
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.validate();
		frame.setVisible(true);
		return frame;
	}
	
	void updateFrame(JFrame frame, PiecePart pp) {
		frame.setSize(pp.getWidth(), pp.getHeight());
		frame.setLocation(pp.getX() * pp.getWidth(), pp.getY() * pp.getHeight());
		frame.validate();
		frame.setVisible(true);
	}
	
}
